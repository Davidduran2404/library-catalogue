function filterBooks() {
    const searchInput = document.getElementById('search').value.toLowerCase();
    const books = document.getElementsByClassName('book');
    
    for (let i = 0; i < books.length; i++) {
        const book = books[i];
        const title = book.getElementsByTagName('h2')[0].innerText.toLowerCase();
        const author = book.getElementsByTagName('p')[0].innerText.toLowerCase();
        const isbn = book.getElementsByTagName('p')[1].innerText.toLowerCase();
        const category = book.getElementsByTagName('p')[2].innerText.toLowerCase();
        
        if (title.includes(searchInput) || author.includes(searchInput) || isbn.includes(searchInput) || category.includes(searchInput)) {
            book.style.display = '';
        } else {
            book.style.display = 'none';
        }
    }
}
